package com.acabezas.viper_pattern_sample.control;

import android.app.Activity;
import android.content.Intent;

import com.acabezas.viper_pattern_sample.login.LoginActivity;

/**
 * Created by alexandercabezas
 */

public class ControlRouterImpl implements ControlContracts.Router {

    private Activity activity;

    public ControlRouterImpl(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void unRegister() {
        activity = null;
    }

    @Override
    public void presentLoginScreen() {
        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivity(intent);
    }
}
