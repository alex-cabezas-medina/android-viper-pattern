package com.acabezas.viper_pattern_sample.control;

import android.app.Activity;

/**
 * Created by alexandercabezas
 */

public class ControlPresenterImpl implements ControlContracts.Presenter{

    private ControlContracts.Interactor interactor;
    private ControlContracts.Router router;

    ControlPresenterImpl(ControlContracts.View view) {
        interactor = new ControlInteractorImpl();
        router = new ControlRouterImpl((Activity) view);
    }

    @Override
    public void onDestroy() {
        interactor.unRegister();
        interactor = null;
        router.unRegister();
        router = null;
    }

    @Override
    public void goToLoginScreen() {
        router.presentLoginScreen();
    }
}
