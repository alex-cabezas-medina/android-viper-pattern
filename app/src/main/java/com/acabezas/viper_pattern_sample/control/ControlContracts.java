package com.acabezas.viper_pattern_sample.control;

/**
 * Created by alexandercabezas
 */

public class ControlContracts {

    interface View {
        void onDestroy();
    }

    interface Presenter {
        void onDestroy();
        void goToLoginScreen();
    }

    interface Interactor {
        void unRegister();
    }


    interface Router {
        void unRegister();
        void presentLoginScreen();
    }
}
