package com.acabezas.viper_pattern_sample.control;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class ControlActivity extends AppCompatActivity implements ControlContracts.View{

    private ControlContracts.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new ControlPresenterImpl(this);
        presenter.goToLoginScreen();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }
}
