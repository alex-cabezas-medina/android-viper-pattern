package com.acabezas.viper_pattern_sample.home;

import android.app.Activity;
import android.content.Intent;

import com.acabezas.viper_pattern_sample.profile.ProfileActivity;

/**
 * Created by alexandercabezas
 */

public class HomeRouterImpl implements HomeContracts.Router{

    private Activity activity;

    public HomeRouterImpl(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void unRegister() {
        activity = null;
    }

    @Override
    public void presentProfileScreen() {
        Intent intent = new Intent(activity, ProfileActivity.class);
        activity.startActivity(intent);
    }
}
