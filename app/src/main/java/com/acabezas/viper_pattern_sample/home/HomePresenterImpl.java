package com.acabezas.viper_pattern_sample.home;

import android.app.Activity;

/**
 * Created by alexandercabezas
 */

public class HomePresenterImpl implements HomeContracts.Presenter{

    private HomeContracts.Interactor interactor;
    private HomeContracts.Router router;

    HomePresenterImpl(HomeContracts.View view) {
        interactor = new HomeInteractorImpl();
        router = new HomeRouterImpl((Activity) view);
    }

    @Override
    public void onDestroy() {
        interactor.unRegister();
        interactor = null;
        router.unRegister();
        router = null;
    }

    @Override
    public void goProfileScreen() {
        router.presentProfileScreen();
    }

}
