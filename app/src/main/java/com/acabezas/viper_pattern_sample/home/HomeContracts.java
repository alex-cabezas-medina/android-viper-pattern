package com.acabezas.viper_pattern_sample.home;

/**
 * Created by alexandercabezas
 */

public class HomeContracts {

    interface View {
        void onDestroy();
    }

    interface Presenter {
        void goProfileScreen();
        void onDestroy();
    }

    interface Interactor {
        void unRegister();
    }

    interface Router {
        void unRegister();
        void presentProfileScreen();
    }

}
