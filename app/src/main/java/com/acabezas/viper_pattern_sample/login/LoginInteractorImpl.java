package com.acabezas.viper_pattern_sample.login;

/**
 * Created by alexandercabezas
 */

public class LoginInteractorImpl implements LoginContracts.Interactor{

    private LoginContracts.InteractorOutput output;

    public LoginInteractorImpl(LoginContracts.InteractorOutput output) {
        this.output = output;
    }

    @Override
    public void loginUser(String userName, String password) {

        if(userName.equals("username") && password.equals("12345")) {
            output.loginResultSuccess();
        } else {
            output.loginResultFail();
        }

    }

    @Override
    public void unRegister() {
        output = null;
    }
}
