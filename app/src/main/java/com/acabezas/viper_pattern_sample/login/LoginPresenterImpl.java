package com.acabezas.viper_pattern_sample.login;

import android.app.Activity;

import com.acabezas.viper_pattern_sample.R;

/**
 * Created by alexandercabezas
 */

public class LoginPresenterImpl implements LoginContracts.Presenter, LoginContracts.InteractorOutput{

    private LoginContracts.View view;
    private LoginContracts.Interactor interactor;
    private LoginContracts.Router router;

    LoginPresenterImpl(LoginContracts.View view) {
        interactor = new LoginInteractorImpl(this);
        router = new LoginRouterImpl((Activity) view);
        this.view = view;
    }

    @Override
    public void onDestroy() {
        interactor.unRegister();
        interactor = null;
        router.unRegister();
        router = null;
    }

    @Override
    public void onLoginButtonPressed(String userName, String password) {
        interactor.loginUser(userName, password);
    }

    @Override
    public void loginResultSuccess() {
        router.presentHomeScreen();
    }

    @Override
    public void loginResultFail() {
        view.showError(R.string.login_failed);
    }
}
