package com.acabezas.viper_pattern_sample.login;

import android.app.Activity;
import android.content.Intent;

import com.acabezas.viper_pattern_sample.home.HomeActivity;

/**
 * Created by alexandercabezas
 */

public class LoginRouterImpl implements LoginContracts.Router{

    private Activity activity;

    public LoginRouterImpl(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void unRegister() {
        activity = null;
    }

    @Override
    public void presentHomeScreen() {
        Intent intent = new Intent(activity, HomeActivity.class);
        activity.startActivity(intent);
    }
}
