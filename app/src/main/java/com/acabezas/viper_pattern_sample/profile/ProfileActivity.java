package com.acabezas.viper_pattern_sample.profile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.acabezas.viper_pattern_sample.R;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
    }
}
